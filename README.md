# Ladybug Racing
Betting, excitement, speed!  Awesome game app for Android. You can play single or with friends (1 - 4 players).  Test your luck.  Bet on the ladybug race and win (or not).  In any case, this is a qualitatively new experience! Watch gameplay  [on YouTube.](https://youtu.be/ekHbqI49-L8)

Inspired by [Slugrace](https://youtu.be/z24eIFsbEl8)

# Prerequisites
- [Python 3.6+](https://www.python.org/)
- [Kivy>=2.0.0](https://kivy.org/#home)
- [KivyMD>=0.104.2](https://kivymd.readthedocs.io/en/latest/)
